package org.alxj.digitalcircuits.console;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.alxj.digitalcircuits.core.API;



public class MainConsole {
	public static void main(String[] args) {
		// Reading models from files
		int id = API.addElement("NAND2");
		API.updateElementText(id, getFromFile("examples/elements/nand2.txt"));
		
		id = API.addElement("NOR2");
		API.updateElementText(id, getFromFile("examples/elements/nor2.txt"));
		
		id = API.addScheme("RS");
		API.updateSchemeText(id, getFromFile("examples/schemes/rs.txt"));

		id = API.addWaveform("WX0");
		API.updateWaveformText(id, getFromFile("examples/waveforms/wx0.txt"));
		
		id = API.addWaveform("WX1");
		API.updateWaveformText(id, getFromFile("examples/waveforms/wx1.txt"));
		
		id = API.addWaveform("WY0");
		API.updateWaveformText(id, getFromFile("examples/waveforms/wy0.txt"));
		
		id = API.addWaveform("WY1");
		API.updateWaveformText(id, getFromFile("examples/waveforms/wy1.txt"));
	
		id = API.addTask("TASK1");
		API.updateTaskText(id, getFromFile("examples/task1.txt"));
		
		// Run
		API.runTask(id);		
		System.out.print(API.collectLogMessages());
	}
	
	private static String getFromFile(String path) {
		File file = new File(path);
		StringBuilder contents = new StringBuilder();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			String text = null;
			while ((text = reader.readLine()) != null)
				contents.append(text).append(System.getProperty("line.separator"));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return contents.toString();
	}
}
