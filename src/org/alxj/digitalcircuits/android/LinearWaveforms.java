package org.alxj.digitalcircuits.android;

import org.alxj.digitalcircuits.core.API;

import android.content.Context;
import android.graphics.Color;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.LinearLayout;

// Contains visuals of several waveforms with common scale and controls
public class LinearWaveforms extends LinearLayout {
	private Context contextParent;	

	private int waveformsCount = 0;
	private SurfaceWaveform[] waveforms = new SurfaceWaveform[10];
	
	public LinearWaveforms(Context context) {
		super(context);
		contextParent = context;
		this.setOrientation(LinearLayout.VERTICAL);
	}
	
	public void startZoom(float middle, int shift) {
		for (int i = 0; i < waveformsCount; ++i)
			waveforms[i].startZoom(middle, shift);	
	}
	
	public void finishZoom() {
		for (int i = 0; i < waveformsCount; ++i)
			waveforms[i].finishZoom();	
	}
	
	public void update(float width, boolean resize) {
		for (int i = 0; i < waveformsCount; ++i)
			waveforms[i].update(width, resize);
	}
	
	public void buildContentTask(String text, float timeUnitWidth) {
		CodeParser parser = new CodeParser(text);
		// Adding all waveforms
		for (;;) {
			if (parser.token.matches(" "))
				break;
			int dbIndex = API.getWaveformIndex(parser.getToken());
			if (dbIndex >= 0)
				AddLinearLayoutWaveform(parser.token, API.getWaveformText(dbIndex), timeUnitWidth);
		}
	}
	
	public void buildContentWaveform(String name, String text, float timeUnitWidth) {
		// Adding this waveform
		AddLinearLayoutWaveform(name, text, timeUnitWidth);
	}
	
	private void AddLinearLayoutWaveform(String name, String text, float timeUnitWidth) {
		SurfaceView waveformVisual = new SurfaceWaveform(contextParent);
		((SurfaceWaveform)waveformVisual).buildContent(name, text, timeUnitWidth);
		waveformVisual.setBackgroundColor(Color.parseColor("#EEEEEE"));
		waveforms[waveformsCount] = (SurfaceWaveform)waveformVisual;
		++waveformsCount;
		this.addView(waveformVisual);
		this.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));
	}
}
