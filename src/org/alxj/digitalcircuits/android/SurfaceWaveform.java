package org.alxj.digitalcircuits.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.LinearLayout.LayoutParams;

// Visuals of a waveform
public class SurfaceWaveform extends SurfaceView implements Callback {
	private int[] values = new int[100];
	private float[] times = new float[100];
	private int switches = 0; 
	private float TIME = 20;

	private SurfaceHolder holder;
	private Paint paintTimeline;
	private Paint paintDefinite;
	private Paint paintIndefinite;
	private Paint paintError;
	private CodeParser parser;
	private float TIME_UNIT_WIDTH = 100;
	private static int X = 2;
	private int[] levelsYs = new int[]{50, 10, 30};
	private boolean zoom = false;
	private float zoomMiddleTime = 0;
	private int zoomShift = 0;
	private float startTime = 0;

	public SurfaceWaveform(Context context) {
		super(context);
		this.setId(100500);
		
		this.setBackgroundColor(Color.parseColor("#EEEEEE"));
		
		holder = getHolder();
		holder.addCallback(this);
		
		paintTimeline = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintTimeline.setStyle(Paint.Style.STROKE);
		paintTimeline.setStrokeWidth(2);
		paintTimeline.setColor(Color.parseColor("#AAAAAA"));
		
		paintDefinite = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintDefinite.setStyle(Paint.Style.STROKE);
		paintDefinite.setStrokeWidth(3);
		paintDefinite.setColor(Color.BLUE);
		
		paintIndefinite = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintIndefinite.setStyle(Paint.Style.FILL_AND_STROKE);
		paintIndefinite.setStrokeWidth(3);
		paintIndefinite.setColor(Color.GRAY);
		
		paintError = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintError.setStyle(Paint.Style.FILL_AND_STROKE);
		paintError.setTextSize(25);
		paintError.setColor(Color.RED);
	}
	
	public void startZoom(float middle, int shift) {
		zoom = true;
		this.setLayoutParams(new LayoutParams(ActivityMain.modelLayoutWidth, 60));
		zoomMiddleTime = middle / TIME_UNIT_WIDTH;
		zoomShift = shift;
	}
	
	public void finishZoom() {
		zoom = false;
		startTime = 0;
		this.setLayoutParams(new LayoutParams(getWaveformWidth(), 60));
	}
	
	private int getWaveformWidth() {
		int timeBasedWidth = (int)(TIME * TIME_UNIT_WIDTH);
		return (timeBasedWidth > ActivityMain.modelLayoutWidth) ? timeBasedWidth 
				: ActivityMain.modelLayoutWidth;
			 
	}
	
	public void update(float timeUnitWidthNew, boolean resize) {
		TIME_UNIT_WIDTH = timeUnitWidthNew;
		if (zoom)
			startTime = zoomMiddleTime - zoomShift / TIME_UNIT_WIDTH;
		else
			startTime = 0;
		invalidate();
	}
	
	public void buildContent(String waveformName, String waveformText, float timeUnitWidth) {
		TIME_UNIT_WIDTH = timeUnitWidth;
		this.setLayoutParams(new LayoutParams(getWaveformWidth(), 60));
		parser = new CodeParser(waveformText);
		for (;;) {
			times[switches] = parser.getTokenFloat();
			if (times[switches] < 0)
				break;
			values[switches] = parser.getTokenInt();
			++switches;
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Canvas canvas = holder.lockCanvas();
		holder.unlockCanvasAndPost(canvas);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// Drawing background
		canvas.drawColor(Color.parseColor("#EEEEEE"));
		for (float i = -startTime; ; ++i)
			if (TIME_UNIT_WIDTH * i >= canvas.getWidth())
				break;
			else
				canvas.drawLine(TIME_UNIT_WIDTH * i, 0, TIME_UNIT_WIDTH * i, canvas.getHeight(), 
						paintTimeline);
		// Drawing waveform
		int levelOld = X;
		int levelNew;
		float timeOld = -startTime;
		float timeNew;
		for (int i = 0; i < switches; ++i){
			timeNew = times[i] - startTime;
			levelNew = values[i];
			// Drawing line from old time to new time at old level
			if (levelOld == 0 || levelOld == 1)
				canvas.drawLine(TIME_UNIT_WIDTH * timeOld, levelsYs[levelOld], TIME_UNIT_WIDTH * timeNew, 
						levelsYs[levelOld], paintDefinite);
			else if (levelOld == X)
				canvas.drawRect(TIME_UNIT_WIDTH * timeOld, levelsYs[X], TIME_UNIT_WIDTH * timeNew, 
						levelsYs[0], paintIndefinite);
			// Drawing transition from old level to new level at new time
			canvas.drawLine(TIME_UNIT_WIDTH * timeNew, levelsYs[levelOld], TIME_UNIT_WIDTH * timeNew, 
				levelsYs[levelNew], paintDefinite);
			// Remembering
			timeOld = timeNew;
			levelOld = levelNew;
		}
		// Drawing last level to end
		if (levelOld == 0 || levelOld == 1)
			canvas.drawLine(TIME_UNIT_WIDTH * timeOld, levelsYs[levelOld], canvas.getWidth(), 
					levelsYs[levelOld], paintDefinite);
		else if (levelOld == X)
			canvas.drawRect(TIME_UNIT_WIDTH * timeOld, levelsYs[X], canvas.getWidth(), levelsYs[0], 
					paintIndefinite);
	}
}


