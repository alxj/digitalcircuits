package org.alxj.digitalcircuits.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.alxj.digitalcircuits.core.API;
import org.alxj.digitalcircuits.gen.R;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityMain extends Activity
{
	public static int modelLayoutWidth = 0;
	private static boolean IS_FIRST_LOAD = true;
	private static int SCROLL_POS = 0;
	
	private static int ACTIVE_TYPE_ID = -1;
	private static int ADD_TYPE_ID = -1;
	private static String ACTIVE_NAME = "";
	
	private void rebuild() {
		// Remembering position
		ScrollView scrollView = (ScrollView) findViewById(R.id.scrollWievControls);
		SCROLL_POS = scrollView.getScrollY();
		onCreate(null);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.gui_layout);
		if (IS_FIRST_LOAD) {
			modelLayoutWidth = getWindowManager().getDefaultDisplay().getWidth() - (int)TypedValue
					.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, getResources().getDisplayMetrics());
			API.deleteAll();
			// Fill the lists with initial models
			addModel(R.id.linearLayoutElements, "OR2", readRawTextFile(this, R.raw.or2));
			addModel(R.id.linearLayoutElements, "AND2", readRawTextFile(this, R.raw.and2));
			addModel(R.id.linearLayoutElements, "NOR2", readRawTextFile(this, R.raw.nor2));
			addModel(R.id.linearLayoutElements, "NAND2", readRawTextFile(this, R.raw.nand2));
			addModel(R.id.linearLayoutSchemes, "RS", readRawTextFile(this, R.raw.rs));
			addModel(R.id.linearLayoutWaveforms, "WY1", readRawTextFile(this, R.raw.wy1));
			addModel(R.id.linearLayoutWaveforms, "WY0", readRawTextFile(this, R.raw.wy0));
			addModel(R.id.linearLayoutWaveforms, "WX1", readRawTextFile(this, R.raw.wx1));
			addModel(R.id.linearLayoutWaveforms, "WX0", readRawTextFile(this, R.raw.wx0));
			addModel(R.id.linearLayoutTasks, "TASK1", readRawTextFile(this, R.raw.task1));
			IS_FIRST_LOAD = false;
		}
		addAllVisuals();
	}
	
	private void addAddButton(final int id, final String titleText) {
		TextView add = (TextView) findViewById(id);
		add.setOnClickListener(new TextView.OnClickListener() {
			int id2 = id;
			String titleText2 = "" + titleText;
			@Override
			public void onClick(View view) {
				ADD_TYPE_ID = id2;
				Intent intent = new Intent(view.getContext(), ActivityTextEditor.class);
				intent.putExtra("TEXT", "");
				intent.putExtra("TITLE", titleText2);
				startActivityForResult(intent, 1);
			}
		});
	}
	
	// GUI drawing methods
	private void addAllVisuals() {
		// Add '+' buttons listeners
		addAddButton(R.id.linearLayoutElementsAdd, "new element name");
		addAddButton(R.id.linearLayoutSchemesAdd, "new scheme name");
		addAddButton(R.id.linearLayoutWaveformsAdd, "new waveform name");
		addAddButton(R.id.linearLayoutTasksAdd, "new task name");
		
		// Fill elements
		if (API.getElementCount() == 0)
			addEntry(R.id.linearLayoutElements, "(empty)");
		for (int i = API.getElementCount() - 1; i >= 0; --i) {
			addEntry(R.id.linearLayoutElements, API.getElementName(i));
			// setEntryListener(R.id.linearLayoutElements);
		}
		
		// Fill schemes
		if (API.getSchemeCount() == 0)
			addEntry(R.id.linearLayoutSchemes, "(empty)");
		for (int i = API.getSchemeCount() - 1; i >= 0; --i) {
			addEntry(R.id.linearLayoutSchemes, API.getSchemeName(i));
			// setEntryListener(R.id.linearLayoutElements);
		}
		
		// Fill waveforms
		if (API.getWaveformCount() == 0)
			addEntry(R.id.linearLayoutWaveforms, "(empty)");
		for (int i = API.getWaveformCount() - 1; i >= 0; --i) {
			addEntry(R.id.linearLayoutWaveforms, API.getWaveformName(i));
			// setEntryListener(R.id.linearLayoutElements);
		}
		
		// Fill tasks
		if (API.getTaskCount() == 0)
			addEntry(R.id.linearLayoutTasks, "(empty)");
		for (int i = API.getTaskCount() - 1; i >= 0; --i) {
			addEntry(R.id.linearLayoutTasks, API.getTaskName(i));
			// setEntryListener(R.id.linearLayoutElements);
		}
		
		// Scroll to previous position
		final ScrollView scrollView = (ScrollView)findViewById(R.id.scrollWievControls);
		scrollView.post(new Runnable() {
			@Override
			public void run() {
			scrollView.scrollTo(0, SCROLL_POS);
			}
		});
		
		// Draw model
		String title = "" + ACTIVE_NAME;
		switch (ACTIVE_TYPE_ID) {
		case R.id.linearLayoutElements:
			addModelElement(API.getElementText(API.getElementIndex(ACTIVE_NAME)));
			title += " (element)";
			break;
		case R.id.linearLayoutSchemes:
			addModelScheme(API.getSchemeText(API.getSchemeIndex(ACTIVE_NAME)));
			title += " (scheme)";
			break;
		case R.id.linearLayoutWaveforms:
			addModelWaveform(API.getWaveformText(API.getWaveformIndex(ACTIVE_NAME)));
			title += " (waveform)";
			break;
		case R.id.linearLayoutTasks:
			addModelTask(API.getTaskText(API.getTaskIndex(ACTIVE_NAME)));
			title += " (task)";
			break;
		default:
			addModelEmpty();
			break;
		}

		// Show title
		LinearLayout layout = (LinearLayout) findViewById(R.id.linearLayoutTitle);
		TextView titleText = new TextView(this);
		titleText.setText(title);
		titleText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		titleText.setTextColor(Color.rgb(255, 255, 255));
		titleText.setGravity(Gravity.CENTER_VERTICAL);
		layout.addView(titleText);
		
		// Show 'Play' button for task
		if (ACTIVE_TYPE_ID == R.id.linearLayoutTasks) {
			Button playButton = new Button(this);
			playButton.setText("Play");
			playButton.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));
			playButton.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (API.runTask(API.getTaskIndex(ACTIVE_NAME)) < 0)
						Toast.makeText(ActivityMain.this, "Error in task!", Toast.LENGTH_SHORT).show();
					rebuild();
				}
			});
			layout.addView(playButton);
		}
	}
	
	public void addModelElement(String text) {
		// Add element visual
		
		// Add element text
		addCodeText(text);
		// Log
		addLog();
	}
	
	public void addModelScheme(String text) {
		// Add scheme visual
		
		// Add code text
		addCodeText(text);
		// Log
		addLog();
	}
	
	public void addModelWaveform(String text) {
		// Add waveform
		HorizontalScrollView scroll = new ScrollWaveforms(this);
		((ScrollWaveforms)scroll).buildContentWaveform(ACTIVE_NAME, text);
		((LinearLayout) findViewById(R.id.linearLayoutModel)).addView(scroll);
		// Add code text
		addCodeText(text);
		// Log
		addLog();
	}
	
	public void addModelTask(String text) {
		// Add task waveforms
		HorizontalScrollView scroll = new ScrollWaveforms(this);
		((ScrollWaveforms)scroll).buildContentTask(text);
		((LinearLayout) findViewById(R.id.linearLayoutModel)).addView(scroll);
		// Add task text
		addCodeText(text);
		// Log
		addLog();
	}
	
	public void addModelEmpty() {
		// Log
		addLog();
	}
	
	public void addLog() {
		LinearLayout parentLayout = (LinearLayout) findViewById(R.id.linearLayoutModel);
		
		ScrollView scrollViewLog = new ScrollView(this);
		scrollViewLog.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));
		
		LinearLayout logLayout = new LinearLayout(this);
		logLayout.setOrientation(LinearLayout.VERTICAL);
		logLayout.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));
		
		TextView logTitle = new TextView(this);
		logTitle.setTextColor(Color.rgb(10, 10, 10));
		logTitle.setBackgroundColor(Color.parseColor("#AAAA99"));
		logTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		logTitle.setText("Log");
		logTitle.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 
				ViewGroup.LayoutParams.WRAP_CONTENT));
		logLayout.addView(logTitle);
		
		TextView logText = new TextView(this);
		logText.setTextColor(Color.rgb(10, 10, 10));
		logText.setBackgroundColor(Color.parseColor("#DDDDBB"));
		logText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
		logText.setText(API.collectLogMessages());
		logText.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));
		logLayout.addView(logText);

		scrollViewLog.addView(logLayout);
		
		parentLayout.addView(scrollViewLog);
	}
	
	private void addCodeText(final String text) {
		LinearLayout layout = (LinearLayout) findViewById(R.id.linearLayoutModel);
		TextView modelText = new TextView(this);
		modelText.setTextColor(Color.rgb(10, 10, 10));
		modelText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		modelText.setText(text);
		modelText.setClickable(true);
		modelText.setOnClickListener(new TextView.OnClickListener() {
			String text2 = "" + text;
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(view.getContext(), ActivityTextEditor.class);
				intent.putExtra("TEXT", text2);
				intent.putExtra("TITLE", ACTIVE_NAME);
				startActivityForResult(intent, 1);
			}
		});
		layout.addView(modelText);
	}
	
	private void addEntry(final int id, String text) {
		LinearLayout layout = (LinearLayout) findViewById(id);
		TextView modelText = new TextView(this);
		modelText.setText(text);
		modelText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		modelText.setClickable(true);
		if (id == ACTIVE_TYPE_ID && text.matches(ACTIVE_NAME))
			modelText.setTextColor(Color.rgb(75, 255, 80));
		modelText.setOnClickListener(new TextView.OnClickListener() {
			int id2 = id;
			@Override
			public void onClick(View view) {
				ACTIVE_TYPE_ID = id2;
				ACTIVE_NAME = "" + ((TextView)view).getText();
				rebuild();
			}
		});
		layout.addView(modelText);
	}
	
	// No GUI, 
	private int addModel(int id, String modelName, String modelText) {
		// Adding empty model to database
		int dbIndex = -1;
		switch (id) {
		case R.id.linearLayoutElements:
			dbIndex = API.addElement(modelName);
			API.updateElementText(dbIndex, modelText);
			break;
		case R.id.linearLayoutSchemes:
			dbIndex = API.addScheme(modelName);
			API.updateSchemeText(dbIndex, modelText);
			break;
		case R.id.linearLayoutWaveforms:
			dbIndex = API.addWaveform(modelName);
			API.updateWaveformText(dbIndex, modelText);
			break;
		case R.id.linearLayoutTasks:
			dbIndex = API.addTask(modelName);
			API.updateTaskText(dbIndex, modelText);
			break;
		}
		return dbIndex;
	}
	
	public static String readRawTextFile(Context ctx, int resId)
    {
         InputStream inputStream = ctx.getResources().openRawResource(resId);

            InputStreamReader inputreader = new InputStreamReader(inputStream);
            BufferedReader buffreader = new BufferedReader(inputreader);
             String line;
             StringBuilder text = new StringBuilder();

             try {
               while (( line = buffreader.readLine()) != null) {
                   text.append(line);
                   text.append('\n');
                 }
           } catch (IOException e) {
               return null;
           }
             return text.toString();
    }
	
	// Text editor activity result
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data == null)
			return;
		// If model text returns
		if (ADD_TYPE_ID < 0)
			switch (ACTIVE_TYPE_ID) {
			case R.id.linearLayoutElements:
				API.updateElementText(API.getElementIndex(ACTIVE_NAME), data.getStringExtra("TEXT"));
				break;
			case R.id.linearLayoutSchemes:
				API.updateSchemeText(API.getSchemeIndex(ACTIVE_NAME), data.getStringExtra("TEXT"));
				break;
			case R.id.linearLayoutWaveforms:
				API.updateWaveformText(API.getWaveformIndex(ACTIVE_NAME), data.getStringExtra("TEXT"));
				break;
			case R.id.linearLayoutTasks:
				API.updateTaskText(API.getTaskIndex(ACTIVE_NAME), data.getStringExtra("TEXT"));
				break;
			}
		if (ADD_TYPE_ID < 0) {
			rebuild();
			return;
		}
		// If model name returns
		String name = new CodeParser(data.getStringExtra("TEXT")).getToken();
		if (name.matches(" ")) {
			Toast.makeText(this, "Empty name", Toast.LENGTH_SHORT).show();
			ADD_TYPE_ID = -1;
			rebuild();
			return;
		}
		int dbIndexNew = -1;
		String notificationText = "";
		switch (ADD_TYPE_ID) {
		case R.id.linearLayoutElementsAdd:
			dbIndexNew = API.addElement(name);
			notificationText = "Element";
			break;
		case R.id.linearLayoutSchemesAdd:
			dbIndexNew = API.addScheme(name);
			notificationText = "Scheme";
			break;
		case R.id.linearLayoutWaveformsAdd:
			dbIndexNew = API.addWaveform(name);
			notificationText = "Waveform";
			break;
		case R.id.linearLayoutTasksAdd:
			dbIndexNew = API.addTask(name);
			notificationText = "Task";
			break;
		}
		if (dbIndexNew < 0)
			Toast.makeText(this, notificationText +" with name " + name + " already exists", 
					Toast.LENGTH_SHORT).show();
		ADD_TYPE_ID = -1;
		rebuild();
	}
	
	@Override
	public void onBackPressed() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}
}