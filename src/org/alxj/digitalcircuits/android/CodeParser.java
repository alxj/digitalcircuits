package org.alxj.digitalcircuits.android;

public class CodeParser {
	public String token = " ";
	
	private int pos = 0;
	private char[] chars;
	private String text;
	private int length = 0;
	
	public CodeParser(String text_) {
		if (text_ == null)
			text_ = " ";
		text = "";
		token = "";
		pos = 0;
		chars = text_.toCharArray();
		length = text_.length();
		for (int i = 0; i < length; ++i) {
			if (chars[i] >= 'a' && chars[i] <= 'z')
				chars[i] += 'A' - 'a';
			text += chars[i];
		}
	}
	
	public boolean lookupApostroph() {
		if (pos < length && chars[pos] == '\'')
			return true;
		return false;
	}
	
	public float getTokenFloat() {
		getToken();
		float value = 0;
		try {
			value = Float.parseFloat(token);
		} catch (NumberFormatException ex) {
			return -1;
		}
		return value;
	}
	
	public int getTokenInt() {
		getToken();
		int value = 0;
		try {
			value = Integer.parseInt(token);
		} catch (NumberFormatException ex) {
			if (token.matches("X"))
				return 2;
			return -1;
		}
		return value;
	}
	
	public String getToken() {
		// Skipping spaces
		while (pos < length
				&& !(chars[pos] >= 'A' && chars[pos] <= 'Z' || chars[pos] >= '0' && chars[pos] <= '9'
				|| chars[pos] == '.'))
			++pos;
		if (pos == length) {
			token = " ";
			return token;
		}
		int startPos = pos;
		// Finding end position of token
		while (pos < length
				&& (chars[pos] >= 'A' && chars[pos] <= 'Z' || chars[pos] >= '0' && chars[pos] <= '9'
				|| chars[pos] == '.'))
			++pos;
		token = text.substring(startPos, pos);
		if (token.length() == 0)
			token = " ";
		return token;
	}
}
