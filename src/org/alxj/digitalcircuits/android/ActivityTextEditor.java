package org.alxj.digitalcircuits.android;

import org.alxj.digitalcircuits.gen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityTextEditor extends Activity {
	EditText editText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.textedit_layout);
		Intent intent = getIntent();
		if (intent == null)
			return;
		
		// Setting editor title
		TextView editTitle = (TextView) findViewById(R.id.textViewTitle);
		editTitle.setText("Editing " + intent.getStringExtra("TITLE"));
		
		// Setting editor text
		editText = (EditText) findViewById(R.id.textEditCode);
		editText.setText(intent.getStringExtra("TEXT"));
		
		// Setting controls
		Button buttonOk = (Button) findViewById(R.id.buttonOk);
		buttonOk.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View view) {
				returnEditText();
			}
		});
	}
	
	private void returnEditText() {
		Intent intent = new Intent();
		intent.putExtra("TEXT", editText.getText().toString());
		setResult(RESULT_OK, intent);
		finish();
	}
	
	@Override
	public void onBackPressed() {
		returnEditText();
	}
}
