package org.alxj.digitalcircuits.android;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
public class ScrollWaveforms extends HorizontalScrollView {
	private Context contextParent;	
	public LinearLayout waveforms;
	
	float startDistance, scale = 1;
	float timeUnitWidthStart;
	float timeUnitWidth = 100;
	int touchState, IDLE = 0, TOUCH = 1, PINCH = 2;
	//private float startTimeShowing = 0;
	//private float maxTime = 10;
	
	public ScrollWaveforms(Context context) {
		super(context);
		contextParent = context;
	}
	
	public void buildContentTask(String text) {
		waveforms = new LinearWaveforms(contextParent);
		((LinearWaveforms) waveforms).buildContentTask(text, timeUnitWidth);
		this.addView(waveforms);
	}
	
	public void buildContentWaveform(String name, String text) {
		waveforms = new LinearWaveforms(contextParent);
		((LinearWaveforms) waveforms).buildContentWaveform(name, text, timeUnitWidth);
		this.addView(waveforms);
	}


	float startMiddle = 0, startShift = 0, startMiddleTime = 0;
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		super.dispatchTouchEvent(event);
		float distx;
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			touchState = TOUCH;
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			touchState = PINCH;
			timeUnitWidthStart = timeUnitWidth;
			distx = event.getX(0) - event.getX(1);
			startDistance = Math.abs(distx);
			startMiddle = this.getScrollX() + event.getX(0)/2 + event.getX(1)/2;
			startMiddleTime = startMiddle / timeUnitWidth;
			startShift = event.getX(0)/2 + event.getX(1)/2;
			// Disabling scroll to do zoom. Sending middle time
			final ScrollWaveforms thisP = this;
			this.post(new Runnable() {
				@Override
				public void run() {
					thisP.scrollTo(0, 0);
					return;
				}
			});
			((LinearWaveforms) waveforms).startZoom(startMiddle, (int)(startShift));
			break;
		case MotionEvent.ACTION_MOVE:    
			if(touchState == PINCH) {      
				distx = event.getX(0) - event.getX(1);
				float tryScale = Math.abs(distx) / startDistance;
				if (timeUnitWidthStart * tryScale < 20 || timeUnitWidthStart * tryScale > 500)
					break;
				scale = tryScale;
				((LinearWaveforms) waveforms).update((float)(timeUnitWidthStart * scale), false);
			}
			break;
		case MotionEvent.ACTION_UP:
			touchState = IDLE;
			break;
		case MotionEvent.ACTION_POINTER_UP:
			touchState = TOUCH;
			if (timeUnitWidthStart * scale < 20 || timeUnitWidthStart * scale > 500)
				break;
			timeUnitWidth = (float)(timeUnitWidthStart * scale);
			// Enabling scroll after zoom
			((LinearWaveforms) waveforms).finishZoom();
			((LinearWaveforms) waveforms).update((float)(timeUnitWidth), true);
			final ScrollWaveforms thisP2 = this;
			final float startPos = (int)(startMiddleTime * timeUnitWidth - startShift);
			this.post(new Runnable() {
				@Override
				public void run() {
					thisP2.scrollTo((int)startPos, 0);
					return;
				}
			});
			break;
		}
		return true;
	}
}
