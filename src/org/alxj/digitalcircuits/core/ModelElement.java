package org.alxj.digitalcircuits.core;

public class ModelElement {
	// Compilable description
	public String text;
	public String name;
	
	public static int LOGICS_BASE = 3;
	public static int MAX_TOTAL_STATES = 5;
	public static int X = 2;
	private static int MAX_ROWS = (int)Math.round(Math.pow(LOGICS_BASE, MAX_TOTAL_STATES));
	
	// Truth table with columns layout
	public int[][] truthTableInputs = new int[MAX_ROWS][10];
	int inputs;
	public int[][] truthTableQPs = new int[MAX_ROWS][10];
	int qStates;
	public int[][] truthTableOutputs = new int[MAX_ROWS][10];
	int outputs;
	public int[][] truthTableQs = new int[MAX_ROWS][10];
	public double[] truthTableDelays = new double[MAX_ROWS];
	public String[] outputsNames = new String[10];
	
	// Outputs indices in the scheme
	public int[] outputsIndices = new int[10];
	
	// Current states
	public int currentState = 0;
	public int defaultState = 0;
	public int[] internalInputsValues = new int[10];
	
	private int currentRows = 0;
	
	// Creation methods	
	public ModelElement(String name_) {
		name = "" + name_;
		Log.info("New element " + name + " created.");
	}
	
	public ModelElement(ModelElement source) {
		name = "" + source.name;
		text = "" + source.text;
	}
	
	public void addRow(int[] rowValues, double delay) {
		if (rowValues.length < inputs + qStates + outputs + qStates || delay < 0 || currentRows >= MAX_ROWS)
			return;
		for (int i = 0; i < inputs; ++i)
			truthTableInputs[currentRows][i] = rowValues[i];
		for (int i = 0; i < qStates; ++i)
			truthTableQPs[currentRows][i] = rowValues[inputs + i];
		for (int i = 0; i < outputs; ++i)
			truthTableOutputs[currentRows][i] = rowValues[inputs + qStates + i];
		for (int i = 0; i < qStates; ++i)
			truthTableQs[currentRows][i] = rowValues[inputs + qStates + outputs + i];
		truthTableDelays[currentRows] = delay;
		++currentRows;
	}
		
	public int getValue(int outputIndex) {
		for (int i = 0; i < currentRows; ++i) {
			boolean found = true;
			for (int j = 0; j < inputs; ++j)
				if (internalInputsValues[j] != truthTableInputs[i][j])
					found = false;
			if (found)
				return truthTableOutputs[i][outputIndex];
		}
		return -1;
	}
	
	public int getValue(int inputIndex, int inputValue, int outputIndex) {
		int[] tempInputsValues = new int[10];
		for (int j = 0; j < inputs; ++j)
			tempInputsValues[j] = internalInputsValues[j];
		tempInputsValues[inputIndex] = inputValue;
		for (int i = 0; i < currentRows; ++i) {
			boolean found = true;
			for (int j = 0; j < inputs; ++j)
				if (tempInputsValues[j] != truthTableInputs[i][j])
					found = false;
			if (found)
				return truthTableOutputs[i][outputIndex];
		}
		return -1;
	}
}
