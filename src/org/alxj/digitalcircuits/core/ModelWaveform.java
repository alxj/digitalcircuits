package org.alxj.digitalcircuits.core;

public class ModelWaveform {
	// Compilable description
	public String 	text;

	// Switches
	public static int 	X = 2;
	public double[] 	times = new double[100];
	public int[] 		newValues = new int[100];
	public int			switches = 0;
	
	public String 		name = "";
	public boolean		isOutput = false;
	public int 			schemePort = -1;
	
	// Creation methods
	public ModelWaveform(String name_) {
		name = "" + name_;
		Log.info("New waveform " + name + " created.");
	}
	
	public ModelWaveform(ModelWaveform source) {
		name = "" + source.name;
		text = "" + source.text;
	}
	
	public ModelWaveform(ModelWaveform source, int isOutput) {
		name = "" + source.name;
	}
	
	public void attachPort(int port) {
		schemePort = port;
	}
	
	public void addSwitch(double time, int newValue) {
		times[switches] = time;
		newValues[switches] = newValue;
		// todo: check and rearrange switches
		++switches;
	}
	
	// Nearest switch time getter
	public double getNearestSwitchTime(double currentTime) {
		double nearestTime = 9000;
		for (int i = 0; i < switches; ++i)
			if (times[i] >= currentTime && times[i] < nearestTime)
				nearestTime = times[i];
		return nearestTime;
	}
}
