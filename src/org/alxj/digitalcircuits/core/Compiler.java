package org.alxj.digitalcircuits.core;

// Creates and verifies data structures for all models from their text descriptions
public class Compiler {
	private String token = " ";
	private int pos = 0;
	private char[] chars;
	private String text;
	private int length = 0;

	public int compileElement(ModelElement element) {
		if (element == null)
			return -1;
		Log.info("Compiling element " + element.name + "...");
		reset(element.text);
		// Clean element
		for (int i = 0; i < element.outputs; ++i)
			element.outputsNames[i] = "";
		element.outputs = 0;
		element.inputs = 0;
		// Getting entity
		if (! getToken().matches("ENTITY"))
			return postError("Missing keyword 'entity'.");
		if (! getToken().matches("INPUTS"))
			return postError("Missing keyword 'inputs' in entity description.");
		element.inputs = 0;
		for (;;) {
			if (getToken().matches("OUTPUTS"))
				break;
			if (token.matches(" "))
				return postError("Missing keyword 'outputs' in entity description.");
			++element.inputs;
		}
		element.outputs = 0;
		for (;;) {
			if (getToken().matches("ARCHITECTURE"))
				break;
			if (token.matches(" "))
				return postError("Missing keyword 'architecture'.");
			if (getIndex(element.outputsNames, token) >= 0)
				return postError("Output name " + token + " dublicates.");
			element.outputsNames[element.outputs] = "" + token;
			++element.outputs;
		}
		
		// Getting architecture rows
		int[] row = new int[element.inputs + element.outputs];
		int i = 0;
		for (;;) {
			// Getting row for entity
			for (i = 0; i < element.inputs + element.outputs; ++i) {
				getToken();
				if (token.matches(" ")) {
					if (i > 0)
						return postError("Truth table entries quantity does not match entity.");
					break;
				} else if (token.matches("0"))
					row[i] = 0;
				else if (token.matches("1"))
					row[i] = 1;
				else if (token.matches("X"))
					row[i] = 2;
				else
					return postError("Invalid entry found in truth table.");
			}
			// Getting delay time
			getToken();
			if (token.matches(" "))
				// Unfinished line case
				if (i > 0)
					return postError("Missing delay time in truth table.");
				// New empty line case
				else
					break;
			double delayTime = 0;
			try {
				delayTime = Double.parseDouble(token);
			} catch (NumberFormatException ex) {
				return postError("Non-numeric delay time found in truth table.");
			}
			if (delayTime < 0)
				return postError("Negative delay time found in truth table.");
			// Adding row
			element.addRow(row, delayTime);
		}
		// Setting initial state
		for (i = 0; i < element.inputs; ++i)
			element.internalInputsValues[i] = 2;
		// todo: Testing element
		Log.info("No errors in " + element.name);
		return 0;
	}

	public int compileScheme(ModelScheme scheme) {
		if (scheme == null)
			return -1;
		Log.info("Compiling scheme " + scheme.name + "...");
		reset(scheme.text);
		String[] inputsNames = new String[100];
		String[] outputsNames = new String[100];
		String[] globalOutputsNames = new String[100];
		String[] elementsNames = new String[100];
		int[] elementsAccessed = new int[100];
		int[] globalOutputsAccessed = new int[100];
		int[] firstOutputsOfElements = new int[100];
		
		// ENTITY
		if (! getToken().matches("ENTITY"))
			return postError("Missing keyword 'entity'.");
		if (! getToken().matches("INPUTS"))
			return postError("Missing keyword 'inputs' in entity description.");
		scheme.globalInputsN = 0;
		for (;;) {
			if (getToken().matches("OUTPUTS"))
				break;
			if (token.matches(" "))
				return postError("Missing keyword 'outputs' in entity description.");
			scheme.globalInputsIndices[scheme.globalInputsN] = scheme.outputsN;
			if (getIndex(outputsNames, token) >= 0)
				return postError("Global input " + token + " dublicates in scheme.");
			outputsNames[scheme.outputsN] = "" + token;
			++scheme.globalInputsN;
			++scheme.outputsN;
		}
		scheme.globalOutputsN = 0;
		for (;;) {
			if (getToken().matches("ARCHITECTURE"))
				break;
			if (token.matches(" "))
				return postError("Missing keyword 'architecture'.");
			if (getIndex(globalOutputsNames, token) >= 0)
				return postError("Global output " + token + " dublicates in scheme.");
			globalOutputsNames[scheme.globalOutputsN] = "" + token;
			scheme.globalOutputsNames[scheme.globalOutputsN] = "" + token;
			++scheme.globalOutputsN;
		}
		
		// ELEMENTS
		if (! getToken().matches("ELEMENTS"))
			return postError("Missing keyword 'elements'.");
		scheme.elementsList.clear();
		for (;;) {
			// Getting name given to element in scheme
			if (getToken().matches("INPUTS"))
				break;
			else if (token.matches(" "))
				return postError("Missing keyword 'inputs' in architecture description.");
			if (getIndex(elementsNames, token) >= 0)
				return postError("Element " + token + " dublicated in elements declaration.");
			elementsNames[scheme.elementsN] = "" + token;
			scheme.elementsNames[scheme.elementsN] = "" + token;
			++scheme.elementsN;
			if (! getToken().matches("IS"))
				return postError("Missing keyword 'is' in element " + elementsNames[scheme.elementsN] 
						+ " declaration.");
			// Importing element to scheme
			if (getToken().matches(" "))
				return postError("Element name expected after keyword 'is'.");
			int dbElementIndex = API.getElementIndex(token);
			if (dbElementIndex < 0)
				return postError("Element " + token + " not found in database.");
			scheme.elementsList.add(new ModelElement(Database.getElement(dbElementIndex)));
			// Compiling element
			Log.info("Element " + elementsNames[scheme.elementsN - 1] + ":");
			if (new Compiler().compileElement(scheme.elementsList.get(scheme.elementsN - 1)) < 0)
				return postError("Element " + elementsNames[scheme.elementsN - 1] + " of type " 
						+ token + " in scheme has errors.");
			// Adding outputs
			int elementIndex = scheme.elementsList.size() - 1;
			firstOutputsOfElements[scheme.elementsN - 1] = scheme.outputsN;
			for (int i = 0; i < scheme.elementsList.get(elementIndex).outputs; ++i) {
				scheme.elementsList.get(elementIndex).outputsIndices[i] = scheme.outputsN;
				++scheme.outputsN;
			}
		}
		
		// INPUTS
		for (;;) {
			if (getToken().matches("OUTPUTS"))
				break;
			else if (token.matches(" ")) {
				for (int i = 0; i < scheme.elementsN; ++i)
					if (elementsAccessed[i] != 1)
						return postError("Inputs description for element " + elementsNames[i] 
								+ " not found.");
				break;
			}
			int elementIndex = getIndex(elementsNames, token);
			if (elementIndex < 0)
				return postError("Element " + token + " not found in elements list.");
			if (elementsAccessed[elementIndex] > 0)
				return postError("Inputs description of element " + token + " dublicated.");
			elementsAccessed[elementIndex] = 1;
			if (! getToken().matches("FROM"))
				return postError("Missing keyword 'from' in inputs description for element " 
					+ elementsNames[elementIndex]);
			// Processing every input of element
			for (int j = 0; j < scheme.elementsList.get(elementIndex).inputs; ++j) {
				getToken();
				if (token.matches(" ") || token.matches("OUTPUTS"))
					return postError("Too few inputs for element " + elementsNames[elementIndex]);
				// Getting element's output index
				int outputIndex = -1;
				if (lookupApostroph() == false) { // global input
					outputIndex = getIndex(outputsNames, token);
					if (outputIndex < 0 || outputIndex >= scheme.globalInputsN)
						return postError("Unknown global input " + token + " found in scheme.");
				} else { // input from element
					outputIndex = getIndex(elementsNames, token);
					if (outputIndex < 0 || outputIndex >= scheme.globalInputsN)
						return postError("Unknown element " + token + " found in scheme.");
					int elementOutputIndex = getIndex(scheme.elementsList.get(elementIndex).outputsNames, 
							getToken());
					if (elementOutputIndex < 0)
						return postError("No output " + token + " in element " + elementsNames[outputIndex]);
					// Finding true outputIndex
					outputIndex = firstOutputsOfElements[outputIndex] + elementOutputIndex;
				}
				// Assigning output to input
				scheme.links[outputIndex][scheme.inputsN] = 1;
				// Adding input
				scheme.inputsElementsIndices[scheme.inputsN] = elementIndex;
				inputsNames[scheme.inputsN] = "" + token;
				++scheme.inputsN;
			}
		}
		
		// OUTPUTS (global)
		if (! token.matches("OUTPUTS"))
			return postError("Missing keyword 'outputs' in global outputs description.");
		for (int i = 0; i < scheme.globalOutputsN; ++i) {
			if (getToken().matches(" "))
				return postError("Too few global outputs are discribed in scheme.");
			int globalOutputIndex = getIndex(globalOutputsNames, token);
			if (globalOutputIndex < 0 || globalOutputIndex >= scheme.globalOutputsN)
				return postError("Output " + token + " not found in scheme outputs list.");
			if (globalOutputsAccessed[globalOutputIndex] > 0)
				return postError("Output " + token + " of scheme dublicated in its description.");
			globalOutputsAccessed[globalOutputIndex] = 1;
			if (! getToken().matches("IS"))
				return postError("Missing keyword 'is' in connection description for element " 
					+ globalOutputsNames[globalOutputIndex]);
			// Getting element's index
			getToken();
			int outputIndex = -1;
			if (lookupApostroph() == false) { // global input
				outputIndex = getIndex(outputsNames, token);
				if (outputIndex < 0 || outputIndex >= scheme.globalInputsN)
					return postError("Unknown global input " + token + " found in scheme.");
			} else { // input from element
				int elementIndex = getIndex(elementsNames, token);
				if (elementIndex < 0)
					return postError("Element not found for scheme output " 
							+ globalOutputsNames[globalOutputIndex]);
				// Getting element's output index
				outputIndex = getIndex(elementsNames, token);
				if (outputIndex < 0 || outputIndex >= scheme.globalInputsN)
					return postError("Unknown element " + token + " found in scheme.");
				int elementOutputIndex = getIndex(scheme.elementsList.get(elementIndex).outputsNames, 
						getToken());
				if (elementOutputIndex < 0)
					return postError("No output " + token + " in element " + elementsNames[outputIndex]);
				// Finding true outputIndex
				outputIndex = firstOutputsOfElements[elementIndex] + elementOutputIndex;
			}
			// Setting reference of global output
			scheme.globalOutputsIndices[globalOutputIndex] = outputIndex;
		}
		// Checking for extra tokens
		if (! getToken().matches(" "))
			return postError("Unusable word " + token + " found after scheme description.");
		// todo: check scheme
		Log.info("No errors in " + scheme.name);
		return 0;
	}
	
	public int compileWaveform(ModelWaveform waveform) {
		if (waveform == null)
			return -1;
		Log.info("Compiling waveform " + waveform.name + "...");
		reset(waveform.text);
		waveform.switches = 0;
		double currentTime = -1;
		// Getting switches rows
		for (;;) {
			// Getting time
			if (getToken().matches(" "))
				break;
			double switchTime = -2;
			try {
				switchTime = Double.parseDouble(token);
			} catch (NumberFormatException ex) {
				return postError("Non-numeric switch time found in waveform.");
			}
			if (switchTime <= currentTime)
				return postError("Non-sequential switch time found in waveform after time " + currentTime);
			if (waveform.switches == 0 && switchTime > 0) {
				waveform.times[0] = 0;
				waveform.newValues[0] = 2;
				waveform.switches = 1;
				currentTime = 0;
			}
			waveform.times[waveform.switches] = switchTime;
			// Getting value
			if (getToken().matches(" "))
				return postError("Missing value in waveform for time " + switchTime);
			else if (token.matches("0"))
				waveform.newValues[waveform.switches] = 0;
			else if (token.matches("1"))
				waveform.newValues[waveform.switches] = 1;
			else if (token.matches("X"))
				waveform.newValues[waveform.switches] = 2;
			else
				return postError("Invalid value found in waveform for time " + switchTime);
			currentTime = switchTime;
			++waveform.switches;
		}
		Log.info("No errors in " + waveform.name);
		return 0;
	}
	
	public int compileTask(ModelTask task) {
		if (task == null)
			return -1;
		Log.info("Compiling task " + task.name + "...");
		reset(task.text);
		String[] waveformsNames = new String[100];
		// Getting scheme
		if (! getToken().matches("SCHEME"))
			return postError("Missing keyword 'scheme'.");
		if (getToken().matches(" "))
			return postError("Missing scheme name.");
		int dbSchemeIndex = Database.getSchemeIndex(token);
		if (dbSchemeIndex < 0)
			return postError("No scheme " + token + " in database.");
		task.scheme = new ModelScheme(Database.getScheme(dbSchemeIndex));
		if (new Compiler().compileScheme(task.scheme) < 0)
			return postError("Scheme " + task.scheme.name + " in task has errors.");
		if (! getToken().matches("INPUTS"))
			return postError("Missing keyword 'inputs' in entity description.");
		// Adding input waveforms
		for (int i = 0; i < task.scheme.globalInputsN; ++i) {
			getToken();
			if (token.matches("OUTPUTS") || token.matches(" "))
				return postError("Too few input waveforms for scheme " + task.scheme.name);
			int dbWaveformIndex = API.getWaveformIndex(token);
			if (dbWaveformIndex < 0)
				return postError("No waveform " + token + " in database.");
			waveformsNames[i] = "" + token;
			// Now adding this waveform to task
			task.waveformsList.add(new ModelWaveform(Database.getWaveform(dbWaveformIndex)));
			if (new Compiler().compileWaveform(task.waveformsList.get(task.waveformsList.size() - 1)) < 0)
				return postError("Input waveform " + token + " has errors.");
			task.waveformsList.get(task.waveformsList.size() - 1).attachPort(i);
			task.waveformsList.get(task.waveformsList.size() - 1).isOutput = false;
		}
		// Getting output waveforms
		if (! getToken().matches("OUTPUTS"))
			return postError("Missing keyword 'outputs' in task.");
		int outputsCount = 0;
		for (;;) {
			if (getToken().matches("TIME"))
				break;
			if (token.matches(" "))
				return postError("Missing keyword 'time' in task.");
			int dbWaveformIndex = API.getWaveformIndex(token);
			if (dbWaveformIndex < 0)
				return postError("No waveform " + token + " in database.");
			// Check unique waveform for output
			if (getIndex(waveformsNames, token) >= 0)
				return postError("Output waveform " + token + " is used more then once.");
			waveformsNames[outputsCount + task.scheme.globalInputsN] = "" + token;
			++outputsCount;
			// Now adding this waveform to task, empty (because it is output)
			task.waveformsList.add(new ModelWaveform(Database.getWaveform(dbWaveformIndex), 1));
			if (new Compiler().compileWaveform(task.waveformsList.get(task.waveformsList.size() - 1)) < 0)
				return postError("Output waveform " + token + " has errors.");
			task.waveformsList.get(task.waveformsList.size() - 1).isOutput = true;
			if (! getToken().matches("FROM"))
				return postError("Missing keyword 'from' in output waveforms description in task.");
			// Getting and attaching output port
			if (getToken().matches(" "))
				return postError("Missing port name for output waveform " 
						+ waveformsNames[outputsCount + task.scheme.globalInputsN - 1]);
			int outputPortIndex = -1;
			if (lookupApostroph() == false) { // global output
				int globalOutputIndex = getIndex(task.scheme.globalOutputsNames, token);
				if (globalOutputIndex < 0)
					return postError("No global output " + token + " in scheme " + task.scheme.name);
				outputPortIndex = task.scheme.globalOutputsIndices[globalOutputIndex];
			} else { // element
				int elementIndex = getIndex(task.scheme.elementsNames, token);
				if (elementIndex < 0)
					return postError("No element " + token + " in scheme " + task.scheme.name);
				int elementOutputIndex = getIndex(task.scheme.elementsList.get(elementIndex).outputsNames, 
						getToken());
				if (elementOutputIndex < 0)
					return postError("No output " + token + " in element " 
							+ task.scheme.elementsList.get(elementIndex).outputsNames[elementOutputIndex]
							+ " in scheme " + task.scheme.name);
				outputPortIndex 
					= task.scheme.elementsList.get(elementIndex).outputsIndices[elementOutputIndex];
			}
			task.waveformsList.get(task.waveformsList.size() - 1).attachPort(outputPortIndex);
		}
		// Getting time
		try {
			task.maxTime = Double.parseDouble(getToken());
		} catch (NumberFormatException ex) {
			return postError("Non-numeric simulation time found in task.");
		}
		if (task.maxTime < 0)
			return postError("Negative simulation time found in task.");
		// Checking for extra tokens
		if (! getToken().matches(" "))
			return postError("Unusable word " + token + " found after task description.");
		Log.info("No errors in " + task.name);
		return 0;
	}
	
	private int getIndex(String[] array, String value) {
		for (int i = 0; i < array.length; ++i)
			if (array[i] != null && array[i].matches(value))
				return i;
		return -1;
	}
	
	private void reset(String text_) {
		if (text_ == null)
			text_ = " ";
		text = "";
		token = "";
		pos = 0;
		chars = text_.toCharArray();
		length = text_.length();
		for (int i = 0; i < length; ++i) {
			if (chars[i] >= 'a' && chars[i] <= 'z')
				chars[i] += 'A' - 'a';
			text += chars[i];
		}
	}
	
	private boolean lookupApostroph() {
		if (pos < length && chars[pos] == '\'')
			return true;
		return false;
	}
	
	private String getToken() {
		// Skipping spaces
		while (pos < length
				&& !(chars[pos] >= 'A' && chars[pos] <= 'Z' || chars[pos] >= '0' && chars[pos] <= '9'
				|| chars[pos] == '.'))
			++pos;
		if (pos == length) {
			token = " ";
			return token;
		}
		int startPos = pos;
		// Finding end position of token
		while (pos < length
				&& (chars[pos] >= 'A' && chars[pos] <= 'Z' || chars[pos] >= '0' && chars[pos] <= '9'
				|| chars[pos] == '.'))
			++pos;
		token = text.substring(startPos, pos);
		if (token.length() == 0)
			token = " ";
		return token;
	}
	
	private int postError(String text) {
		Log.error(text);
		return -1;
	}
}
