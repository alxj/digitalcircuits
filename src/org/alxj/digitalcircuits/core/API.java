package org.alxj.digitalcircuits.core;

// Provides public API for modeling tasks, schemes, logical elements and waveforms management
public class API {
	
	static {
		Log.info("Digital Circuits core started: version " + Log.getVersion());
	}
	
	private static int postError(String text) {
		Log.error(text);
		return -1;
	}
	
// API: general
	
	public static String collectLogMessages() {
		return Log.popAllMessages();
	}
	
	public static void flushLogMessages() {
		Log.flushAllMessages();
	}
	
	public static int deleteAll() {
		return Database.deleteAll();
	}
	
	public static String getVersion() {
		return Log.getVersion();
	}
	
	public static int runTask(int id) {
		ModelTask task = Database.getTask(id);
		return (task == null) ? -1 : Engine.runTask(task);
	}
	
// Add
	
	public static int addElement(String name) {
		int i = Database.addElement(name);
		return (i >= 0) ? i : (postError("Element " + name + " already exists in database."));
	}
	
	public static int addScheme(String name) {
		int i = Database.addScheme(name);
		return (i >= 0) ? i : (postError("Scheme " + name + " already exists in database."));
	}
	
	public static int addWaveform(String name) {
		int i = Database.addWaveform(name);
		return (i >= 0) ? i : (postError("Waveform " + name + " already exists in database."));
	}
	
	public static int addTask(String name) {
		int i = Database.addTask(name);
		return (i >= 0) ? i : (postError("Task " + name + " already exists in database."));
	}
	
// Update text
	
	public static int updateElementText(int id, String text) {
		ModelElement model = Database.getElement(id);
		if (model == null)
			return -1;
		model.text = text;
		return 0;
	}
	
	public static int updateSchemeText(int id, String text) {
		ModelScheme model = Database.getScheme(id);
		if (model == null)
			return -1;
		model.text = text;
		return 0;
	}
	
	public static int updateWaveformText(int id, String text) {
		ModelWaveform model = Database.getWaveform(id);
		if (model == null)
			return -1;
		model.text = text;
		return 0;
	}
	
	public static int updateTaskText(int id, String text) {
		ModelTask model = Database.getTask(id);
		if (model == null)
			return -1;
		model.text = text;
		return 0;
	}
	
// Get quantity
	
	public static int getElementCount() {
		return Database.getElementCount();
	}
	
	public static int getSchemeCount() {
		return Database.getSchemeCount();
	}
	
	public static int getWaveformCount() {
		return Database.getWaveformCount();
	}
	
	public static int getTaskCount() {
		return Database.getTaskCount();
	}

// Get index
	
	public static int getElementIndex(String name) {
		return Database.getElementIndex(name);
	}
	
	public static int getSchemeIndex(String name) {
		return Database.getSchemeIndex(name);
	}
	
	public static int getWaveformIndex(String name) {
		return Database.getWaveformIndex(name);
	}
	
	public static int getTaskIndex(String name) {
		return Database.getTaskIndex(name);
	}
	
// Get name
	
	public static String getElementName(int id) {
		ModelElement model = Database.getElement(id);
		return (model == null) ? null : (model.name);
	}
	
	public static String getSchemeName(int id) {
		ModelScheme model = Database.getScheme(id);
		return (model == null) ? null : (model.name);
	}
	
	public static String getWaveformName(int id) {
		ModelWaveform model = Database.getWaveform(id);
		return (model == null) ? null : (model.name);
	}
	
	public static String getTaskName(int id) {
		ModelTask model = Database.getTask(id);
		return (model == null) ? null : (model.name);
	}
	
// Get text
	
	public static String getElementText(int id) {
		ModelElement model = Database.getElement(id);
		return (model == null) ? null : ((model.text == null) ? " " : model.text);
	}
	
	public static String getSchemeText(int id) {
		ModelScheme model = Database.getScheme(id);
		return (model == null) ? null : ((model.text == null) ? " " : model.text);
	}
	
	public static String getWaveformText(int id) {
		ModelWaveform model = Database.getWaveform(id);
		return (model == null) ? null : ((model.text == null) ? " " : model.text);
	}
	
	public static String getTaskText(int id) {
		ModelTask model = Database.getTask(id);
		return (model == null) ? null : ((model.text == null) ? " " : model.text);
	}
	
// Compile
	
	public static int compileElement(int id) {
		ModelElement model = Database.getElement(id);
		return (model == null) ? -1 : (new Compiler().compileElement(model));
	}
	
	public static int compileScheme(int id) {
		ModelScheme model = Database.getScheme(id);
		return (model == null) ? -1 : (new Compiler().compileScheme(model));
	}
	
	public static int compileWaveform(int id) {
		ModelWaveform model = Database.getWaveform(id);
		return (model == null) ? -1 : (new Compiler().compileWaveform(model));
	}
	
	public static int compileTask(int id) {
		ModelTask model = Database.getTask(id);
		return (model == null) ? -1 : (new Compiler().compileTask(model));
	}
}
