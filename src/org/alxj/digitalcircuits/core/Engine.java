package org.alxj.digitalcircuits.core;

import java.util.ArrayList;
import java.util.List;

// Conducts simulation of models from tasks
public class Engine {
	
	private static List<Event> eventsList = new ArrayList<Event>();
	private static int[] inputSignals = new int[100];
	private static int[] outputSignals = new int[100];
	public static int MAX_EVENTS = 1000;
	
	public static int runTask(ModelTask task) {
		if (task == null)
			return -1;
		task = new ModelTask(task);
		if (new Compiler().compileTask(task) < 0) {
			Log.info("Running task " + task.name + " aborted because of compilation error.");
			return -1;
		}
		Log.info("Running task " + task.name + "...");
		printDebug(task);
		Log.info("===== Simulation STARTED =====");
		Log.info("Getting events from input waveforms.");
		getEventsFromInputWaveforms(task);
		// Processing events
		for (;;) {
			Log.info("Modeling time = " + task.currentTime);
			for(;;) {
				if (eventsList.size() > MAX_EVENTS) {
					Log.error("Too many events. Running task " + task.name + " stopped.");
					eventsList.clear();
				}
				Event currentEvent = getCurrentEvent(task.currentTime);
				if (currentEvent == null)
					break;			
				if (currentEvent.type == Event.IN)
					processEventIN(currentEvent, task);
				else 
					processEventOUT(currentEvent, task);
				currentEvent.close();
			}
			task.currentTime = getNearestEventTime(task.currentTime, task.maxTime);
			if (task.currentTime > task.maxTime)
				break;
		}
		Log.info("===== Simulation FINISHED =====");
		// Rewriting output waveforms in database
		for (int i = 0; i < task.waveformsList.size(); ++i)
			if (task.waveformsList.get(i).isOutput) {
				int dbIndex = Database.getWaveformIndex(task.waveformsList.get(i).name);
				Database.getWaveform(dbIndex).text = "";
				for (int j = 0; j < task.waveformsList.get(i).switches; ++j) {
					int newValue = task.waveformsList.get(i).newValues[j];
					String newValueText = "" + newValue;
					if (newValue == 2)
						newValueText = "X";
					Database.getWaveform(dbIndex).text += task.waveformsList.get(i).times[j] + " "
							+ newValueText + "\n";
				}
				Log.info("Writing output waveform " + Database.getWaveform(dbIndex).name);
			}
		printDebug2(task);
		Log.info("Done.");
		return 0;
	}
	
	private static void processEventOUT(Event currentEvent, ModelTask task) {
		// Fill in waveforms
		for (int i = 0; i < task.waveformsList.size(); ++i) {
			ModelWaveform waveform = task.waveformsList.get(i);
			if (waveform.isOutput && waveform.schemePort == currentEvent.outputIndex)
				if (waveform.switches == 0 || waveform.switches > 0
					&& task.currentTime > waveform.times[waveform.switches - 1])
					waveform.addSwitch(task.currentTime, currentEvent.outputValue);
		}
		// Updating only output signal
		outputSignals[currentEvent.outputIndex] = currentEvent.outputValue;
		// Then, adding events for predicted input signals
		for (int j = 0; j < task.scheme.inputsN; ++j)
			if (task.scheme.links[currentEvent.outputIndex][j] == 1)
				eventsList.add(new Event(task.currentTime, j, currentEvent.outputValue));
		// Pop
		Log.info("Pop  -> #" + currentEvent.id + ": t = " + currentEvent.time + ", output = " 
				+ currentEvent.outputIndex + ", value = " + currentEvent.outputValue);
	}
	
	private static void processEventIN(Event currentEvent, ModelTask task) {
		// Updating only input signal
		int inputIndex = currentEvent.inputIndex;
		inputSignals[inputIndex] = currentEvent.inputValue;
		// Finding element and its relative input index
		ModelElement element = task.scheme.elementsList.get(task.scheme.inputsElementsIndices[inputIndex]);
		int shift;
		for (shift = 0; shift < task.scheme.inputsN; ++shift)
			if (task.scheme.inputsElementsIndices[shift] == task.scheme.inputsElementsIndices[inputIndex])
				break;
		int relativeInputIndex = inputIndex - shift;
		// Find changes for each element's output between old internal and new external inputs
		for (int j = 0; j < element.outputs; ++j) {
			int oldValue = element.getValue(j);
			int newValue = element.getValue(relativeInputIndex, currentEvent.inputValue, j);
			if (oldValue != newValue)
				eventsList.add(new Event(task.currentTime + element.truthTableDelays[0], 
						element.outputsIndices[j], newValue, 0));
		}
		// Now we can update element's internal inputs
		element.internalInputsValues[relativeInputIndex] = inputSignals[inputIndex];
		// Pop
		Log.info("Pop  >- #" + currentEvent.id + ": t = " + currentEvent.time + ", input = " 
				+ currentEvent.inputIndex + ", value = " + currentEvent.inputValue);
	}
	
	private static void getEventsFromInputWaveforms(ModelTask task) {
	for (int j = 0; j < task.waveformsList.size(); ++j)
		if (!task.waveformsList.get(j).isOutput)
			for (int i = 0; i < task.waveformsList.get(j).switches; ++i)
				eventsList.add(new Event(task.waveformsList.get(j).times[i], 
					task.waveformsList.get(j).schemePort, task.waveformsList.get(j).newValues[i], 0));
	}
	
	private static void printDebug(ModelTask task) {
		Log.debug("inputsElementsIndices:");
		for (int i = 0; i < task.scheme.inputsN; ++i)
			Log.debug("" + task.scheme.inputsElementsIndices[i]);
		Log.debug("global 'inputs' outputs Indices:");
			for (int i = 0; i < task.scheme.globalInputsN; ++i)
				Log.debug("" + task.scheme.globalInputsIndices[i]);
		Log.debug("global outputs Indices:");
			for (int i = 0; i < task.scheme.globalOutputsN; ++i)
				Log.debug("" + task.scheme.globalOutputsIndices[i]);
		for (int j = 0; j < task.scheme.elementsList.size(); ++j) {
		Log.debug("element " + j + " outputs :");
			for (int i = 0; i < task.scheme.elementsList.get(j).outputs; ++i)
				Log.debug("" + task.scheme.elementsList.get(j).outputsIndices[i]);
		}
	}
	
	private static void printDebug2(ModelTask task) {
		int id = 2;
		Log.info("Waveform "+ id + ":");
		for (int i = 0; i < task.waveformsList.get(id).switches; ++i)
			Log.info("t = " + task.waveformsList.get(id).times[i] + ", " 
					+ task.waveformsList.get(id).newValues[i]);
		id = 3;
		Log.info("Waveform "+ id + ":");
		for (int i = 0; i < task.waveformsList.get(id).switches; ++i)
			Log.info("t = " + task.waveformsList.get(id).times[i] + ", " 
					+ task.waveformsList.get(id).newValues[i]);
	}

	public static double getNearestEventTime(double currentTime, double maxTime) {
		int nearestIndex = -1;
		double nearestTime = maxTime;
		for (int i = 0; i < eventsList.size(); ++i)
			if (eventsList.get(i).time > currentTime && eventsList.get(i).time <= nearestTime) {
				nearestTime = eventsList.get(i).time;
				nearestIndex = i;
			}
		if (nearestIndex < 0)
			return maxTime + 1;
		return eventsList.get(nearestIndex).time;
	}
	
	public static Event getCurrentEvent(double currentTime) {
		for (int i = 0; i < eventsList.size(); ++i)
			if (eventsList.get(i).time == currentTime)
				return eventsList.get(i);
		return null;
	}
}
