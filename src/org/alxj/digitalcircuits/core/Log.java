package org.alxj.digitalcircuits.core;

import java.util.ArrayList;
import java.util.List;

public class Log {
	// Version
	private static String major = "2013";
	private static String minor = "02";
	private static String build = "42";
	
	public static boolean DEBUG = true; 
	
	// Stored messages
	public static List<String> messagesList = new ArrayList<String>();
	
	public static void info(String message) {
		messagesList.add("Info: " + message);
	}
	
	public static void warning(String message) {
		messagesList.add("Warning: " + message);
	}
	
	public static void error(String message) {
		messagesList.add("ERROR: " + message);
	}
	
	public static void debug(String message) {
		if (DEBUG)
			messagesList.add("# " + message);
	}
	
	public static String getVersion() {
		return "" + major + "." + minor + " build " + build;
	}
	
	public static String popAllMessages() {
		String answer = "";
		while (messagesList.size() > 0) {
			answer += messagesList.get(0) + "\n";
			messagesList.remove(0);
		}
		return answer;
	}
	
	public static void flushAllMessages() {
		messagesList.clear();
	}
}
