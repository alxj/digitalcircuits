package org.alxj.digitalcircuits.core;

import java.util.ArrayList;
import java.util.List;

public class ModelScheme {
	// Compilable description
	public String 	text;
	
	// Inputs
	public int[] 	inputsElementsIndices = new int[100];
	public int 		inputsN = 0;
	
	// Outputs (global input ports too) - always link to inputs
	public byte[][]	links = new byte[100][100];
	public int[] 	linksQuantities = new int[100];
	public int 		outputsN = 0;
	
	// Globals
	public int[] 	globalInputsIndices = new int[100];
	public int 		globalInputsN = 0;
	public int[] 	globalOutputsIndices = new int[100];
	public int 		globalOutputsN = 0;
	
	public String 	name = "";
	
	public String[] elementsNames = new String[100];
	public String[] globalOutputsNames = new String[100];
	
	// Elements
	public List<ModelElement> elementsList = new ArrayList<ModelElement>();
	public int 		elementsN = 0;
	
	// Creation methods
	public ModelScheme(String name_) {
		name = "" + name_;
		Log.info("New scheme " + name + " created.");
	}
	
	public ModelScheme(ModelScheme source) {
		name = "" + source.name;
		text = "" + source.text;
	}
}
