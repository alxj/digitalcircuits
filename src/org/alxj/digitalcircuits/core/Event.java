package org.alxj.digitalcircuits.core;

// The event is a guaranteed change of the output or global input signal at specified time.
public class Event {

	public static int 	IN = 0,
						OUT = 1,
						COUNTER = 0;
	
	public int 			type = 0;
	public double 		time = 0;
	public int 			inputIndex = 0;
	public int 			inputValue = 0;
	public int 			inputValuePrev = 0;
	public int 			outputIndex = 0;
	public int 			outputValue = 0;
	public int 			outputValuePrev = 0;
	public int 			id = 0;
	
	public static void reset() {
		COUNTER = 0;
	}
	
	// Type OUT: from element
	public Event(double time_, int outputIndex_, int outputValue_, int outBallast) {
		type = OUT;
		time = time_;
		outputIndex = outputIndex_;
		outputValue = outputValue_;
		id = COUNTER;
		Log.info("Push -> #" + id + ": t = " + time + ", output = " + outputIndex + ", value = " + outputValue);
		++COUNTER;
	}
	
	// Type IN: into element
	public Event(double time_, int inputIndex_, int inputValue_) {
		type = IN;
		time = time_;
		inputIndex = inputIndex_;
		inputValuePrev = inputValue;
		inputValue = inputValue_;
		id = COUNTER;
		Log.info("Push >- #" + id + ": t = " + time + ", input = " + inputIndex + ", value = " + inputValue);
		++COUNTER;
	}
	
	public void close() {
		time = -1;
	}
}
