package org.alxj.digitalcircuits.core;

import java.util.ArrayList;
import java.util.List;

public class ModelTask {
	// Compilable description
	public String text;
	public String name;
		
	// Scheme, waveforms, modeling time
	public ModelScheme scheme;
	public List<ModelWaveform> waveformsList = new ArrayList<ModelWaveform>();
	
	public double maxTime = 0;
	public double currentTime = 0;
	
	public ModelTask(String name_) {
		name = "" + name_;
		Log.info("New task " + name + " created.");
	}
	
	public ModelTask(ModelTask source) {
		name = "" + source.name;
		text = "" + source.text;
	}
}
