package org.alxj.digitalcircuits.core;

import java.util.ArrayList;
import java.util.List;

public class Database {
	// Stored objects
	private static List<ModelElement> elementsList = new ArrayList<ModelElement>();
	private static List<ModelScheme> schemesList = new ArrayList<ModelScheme>();
	private static List<ModelWaveform> waveformsList = new ArrayList<ModelWaveform>();
	private static List<ModelTask> modelingTasksList = new ArrayList<ModelTask>();
	
	public static int deleteAll() {
		elementsList.clear();
		schemesList.clear();
		waveformsList.clear();
		modelingTasksList.clear();
		return 0;
	}

// Element
	
	public static int addElement(String name) {
		if (getElementIndex(name) >= 0)
			return -1;
		elementsList.add(new ModelElement(name));
		return Database.elementsList.size() - 1;
	}
	
	public static int getElementCount() {
		return elementsList.size();
	}
	
	public static int getElementIndex(String name) {
		for (int i = 0; i < elementsList.size(); ++i)
			if (elementsList.get(i).name.matches(name))
				return i;
		return -1;
	}
	
	public static ModelElement getElement(int index) {
		if (index < 0 || index >= elementsList.size())
			return null;
		return elementsList.get(index);
	}
	
// Scheme
	
	public static int addScheme(String name) {
		if (getSchemeIndex(name) >= 0)
			return -1;
		schemesList.add(new ModelScheme(name));
		return Database.schemesList.size() - 1;
	}
	
	public static int getSchemeCount() {
		return schemesList.size();
	}
	
	public static int getSchemeIndex(String name) {
		for (int i = 0; i < schemesList.size(); ++i)
			if (schemesList.get(i).name.matches(name))
				return i;
		return -1;
	}
	
	public static ModelScheme getScheme(int index) {
		if (index < 0 || index >= schemesList.size())
			return null;
		return schemesList.get(index);
	}
	
// Waveform
	
	public static int addWaveform(String name) {
		if (getWaveformIndex(name) >= 0)
			return -1;
		waveformsList.add(new ModelWaveform(name));
		return Database.waveformsList.size() - 1;
	}
	
	public static int getWaveformCount() {
		return waveformsList.size();
	}
	
	public static int getWaveformIndex(String name) {
		for (int i = 0; i < waveformsList.size(); ++i)
			if (waveformsList.get(i).name.matches(name))
				return i;
		return -1;
	}
	
	public static ModelWaveform getWaveform(int index) {
		if (index < 0 || index >= waveformsList.size())
			return null;
		return waveformsList.get(index);
	}
	
// Task
	
	public static int addTask(String name) {
		if (getTaskIndex(name) >= 0)
			return -1;
		modelingTasksList.add(new ModelTask(name));
		return Database.modelingTasksList.size() - 1;
	}
	
	public static int getTaskCount() {
		return modelingTasksList.size();
	}
	
	public static int getTaskIndex(String name) {
		for (int i = 0; i < modelingTasksList.size(); ++i)
			if (modelingTasksList.get(i).name.matches(name))
				return i;
		return -1;
	}
	
	public static ModelTask getTask(int index) {
		if (index < 0 || index >= modelingTasksList.size())
			return null;
		return modelingTasksList.get(index);
	}
}
