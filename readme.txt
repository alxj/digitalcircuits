A simple digital circuits simulation app for Android.
Based around the cross-platform core, with API provided.

How to launch:

1. As an Android application:
	Build APK file, using class MainAndroid.
	
2. As a desktop console application:
	Export a Runnable JAR file, using class MainConsole.
